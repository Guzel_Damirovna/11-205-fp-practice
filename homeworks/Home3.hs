
-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = tribo :: Int -> Int
tribo 0 = 1
tribo 1 = 1
tribo 2 = 1
tribo 3 = 3
tribo n = tribo (n-3) + tribo (n-2) + tribo (n-1)


-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции Сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int

normChanel :: Int -> Int
normChanel chanel | chanel > 255 = 255
                  | otherwise = chanel

data Color = Rgb { r :: Int, g :: Int, b :: Int }

white = Rgb 255	255	255
black = Rgb 0	0 	0
red   = Rgb 255 0	0
green = Rgb 0	255	0
blue  = Rgb 0	0	255

addColors :: Color -> Color -> Color
addColors (Rgb r1 g1 b1) (Rgb r2 g2 b2) = Rgb (normChanel(r1 + r2))
                                              (normChanel(g2 + g1))
                                              (normChanel(b1 + b2))

getRedChanel :: Color -> Int
getRedChanel (Rgb r _ _) = r

getBlueChanel :: Color -> Int
getBlueChanel (Rgb _ g _) = g
 
getGreenChanel :: Color -> Int
getGreenChanel (Rgb _ _ b) = b

