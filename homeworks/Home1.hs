{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 n = 4 / (1 + (pi1' n 1))
   where pi1' 1 num = 2
   pi1' n num= (num ^ 2) / (2 + (pi1' (n-1) (num+ 2)))

{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
pi2 n = 3 + pi2' n 1
   where pi2' 1 num = 6
         pi2' n num = (num ^ 2) / (6 + pi2' (n-1) (num + 2))

{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 n = 4 / (1 + (pi3' n 1 3))
    where pi3' 1 num denom = num
          pi3' n num denom = ((num ^ 2) /
             (denom + (pi3' (n-1) (num + 1) (denom + 2))))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 n = -(pi4' n 1)
    where pi4' 0 denom = 0
          pi4' n denom | n `mod` 2 == 0 = -(4 / denom) + pi4' (n-1) (denom+2)
                            | otherwise      = 4 / denom + pi4' (n-1) (denom+2)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 n =  3 + pi5' n 2
    where pi5' 1 denom = 0
          pi5' n denom | n `mod` 2 == 0 = -(4 / (denom * (denom + 1) * (denom + 2))) + pi5' (n-1) (denom+2)
                             | otherwise      =   4 / (denom * (denom + 1) * (denom + 2)) + pi5' (n-1) (denom+2)
