
type Set a = a -> Bool

emptySet :: (Eq a) => Set a
emptySet = \x -> False

singleton :: (Eq a) => a -> Set a
singleton y = \x -> x==y

add :: (Eq a) => a -> Set a -> Set a
add y s = \x -> x==y || s x

contains :: (Eq a) => a -> Set a -> Bool
contains y s = s y

union :: (Eq a) => Set a -> Set a -> Set a
union s1 s2 = \x -> s1 x || s2 x

intersection :: (Eq a) => Set a -> Set a -> Set a
intersection s1 s2 = \x -> s1 x && s2 x

complement :: (Eq a) => Set a -> Set a
complement s = \x -> not (s x)

---------------------------------------------
----  MORSE CODE

type Morse = String

data Tree a = Leaf
            | Branch { tag :: a
                     , left :: Tree a
                     , right :: Tree a
                     }
              deriving (Show)

decode = map decodeLetter . words

program :: String
program = "__5__4H___3VS__F___2 UI__L__+_ R__P___1JWAE"
       ++ "__6__=B__/_XD__C__YKN__7_Z__QG__8_ __9__0 OMT "

-- dict :: Tree Char
-- dict = interpret [] program
--     where interpret stack [] = head stack
--           interpret stack ('_':xs) = interpret (Leaf : stack) xs
--           interpret (x:y:s) (c:xs) = interpret (Branch c y x : s) xs

-- decodeLetter xs = tag $ foldl step dict xs
--     where step dict '.' = left dict
--           step dict '-' = right dict

branch :: Char -> (Morse -> Char) -> (Morse -> Char) -> Morse -> Char
branch c x y = \xs -> case xs of
                        [] -> c
                        ('.':rest) -> x rest
                        ('-':rest) -> y rest

dict :: Morse -> Char
dict = interpret [] program
    where interpret stack [] = head stack
          interpret stack ('_':xs) = interpret (leaf : stack) xs
          interpret (x:y:s) (c:xs) = interpret (branch c y x : s) xs

leaf = undefined

decodeLetter xs = dict xs
    -- where step dict '.' = left dict
    --       step dict '-' = right dict

